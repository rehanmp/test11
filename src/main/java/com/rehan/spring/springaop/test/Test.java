package com.rehan.spring.springaop.test;

import org.springframework.context.ApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

import com.rehan.spring.springaop.ProductService;

public class Test {

	public static void main(String[] args) {
		// TODO Auto-generated method stub
		ApplicationContext context = new ClassPathXmlApplicationContext("rehan/spring/springaop/test/Config.xml");
		ProductService productService = (ProductService) context.getBean("productService");
		System.out.println(productService.multiply(5, 4));
	}

}

package com.rehan.spring.springaop.aspects;

import org.aspectj.lang.JoinPoint;
import org.aspectj.lang.annotation.After;
import org.aspectj.lang.annotation.Aspect;
import org.aspectj.lang.annotation.Before;

@Aspect
public class LoggingAspect {
	@Before("execution(* com.rehan.spring.springaop;.ProductServiceImpl.mulitply(..))")
	public void logBefore(JoinPoint joinPoint) {
		System.out.println("Before calling method");
	}

	@After("execution(* com.rehan.spring.springaop;.ProductServiceImpl.mulitply(..))")
	public void logAfter(JoinPoint joinPoint) {
		System.out.println("After invoking method");
	}

}
